import 'package:flutter/material.dart';
import 'package:shopping_app/model/products_item.dart';

class ComponentProductItem extends StatelessWidget {
  const ComponentProductItem({
    super.key,
    required this.productsItem,
    required this.callback,
  });

  final ProductsItem productsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Row(
          children: [
            SizedBox(
              width: 300,
              height: 300,
              child: Image.asset(productsItem.imgUrl),
            ),
            Column(
              children: [
                Text(productsItem.productTitle),
                Text('${productsItem.productPrice}'),
              ],
            )
          ],
        ),
      ),
    );
  }
}
