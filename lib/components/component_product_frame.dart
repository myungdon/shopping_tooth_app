import 'package:flutter/material.dart';

class ComponentProductFrame extends StatelessWidget {
  const ComponentProductFrame({
    super.key,
    required this.imgUrl,
    required this.productTitle,
    required this.productPrice,
    required this.callback
    
  });
  
  final String imgUrl;
  final String productTitle;
  final num productPrice;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Column(
        children: <Widget>[
          Image.asset(
            imgUrl,
            width: 250,
            height: 250,
            fit: BoxFit.fill,
          ),
          Text(
              productTitle,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
          Text(
              '${productPrice}원',
              style: const TextStyle(fontSize: 18),
          )
        ],
      ),
    );
  }
}
