class ProductsItem {
  num id;
  String imgUrl;
  String productTitle;
  num productPrice;

  // ProductsItem addItem = new ProductsItem(); // 빈 생성자 호출, 생성자 + 오버로딩
  // addItem.setImgUrl()

  ProductsItem(this.id, this.imgUrl, this.productTitle, this.productPrice);
}