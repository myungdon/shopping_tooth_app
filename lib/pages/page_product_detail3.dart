import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const PageProductDetail3());
}

class PageProductDetail3 extends StatefulWidget {
  const PageProductDetail3({Key? key}) : super(key: key);

  @override
  State<PageProductDetail3> createState() => _PageProductDetail3State();
}

class _PageProductDetail3State extends State<PageProductDetail3> {
  String searchValue = '';
  final List<String> _suggestions = [
    '고양이 칫솔',
    '카와이 칫솔',
    '킹갓엠퍼럴 칫솔',
    '개 칫솔',
    '나무 칫솔',
    '칫솔 세트',];

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
        title: '킹갓엠퍼럴 칫솔',
        theme: ThemeData(
            primarySwatch: Colors.orange
        ),
        home: Scaffold(
            appBar: EasySearchBar(
                title: const Text('킹갓엠퍼럴 칫솔'),
                onSearch: (value) => setState(() => searchValue = value),
                suggestions: _suggestions
            ),
            drawer: Drawer(
                child: ListView(
                    padding: EdgeInsets.zero,
                    children: [
                      const DrawerHeader(
                        decoration: BoxDecoration(
                          color: Colors.blue,
                        ),
                        child: Text('Drawer Header'),
                      ),
                      ListTile(
                          title: const Text('Item 1'),
                          onTap: () => Navigator.pop(context)
                      ),
                      ListTile(
                          title: const Text('Item 2'),
                          onTap: () => Navigator.pop(context)
                      )
                    ]
                )
            ),
            body: Center(
              child: Column(
                children: [
                  Image.asset('assets/product3.png'),
                  Text('킹갓엠퍼럴 칫솔'),
                  Text('120000원')
                ],
              ),
            )
        )
    );
  }
}