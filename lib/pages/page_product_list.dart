import 'package:flutter/material.dart';
import 'package:shopping_app/components/component_product_item.dart';
import 'package:shopping_app/model/products_item.dart';
import 'package:shopping_app/pages/page_product_detail.dart';

class PageProductList extends StatefulWidget {
  const PageProductList({super.key});

  @override
  State<PageProductList> createState() => _PageProductListState();
}

class _PageProductListState extends State<PageProductList> {
  final List<ProductsItem> _list = [
    ProductsItem(1, 'assets/product1.png', '카와이 칫솔', 120000),
    ProductsItem(2, 'assets/product2.png', '고양이 칫솔', 120000),
    ProductsItem(3, 'assets/product3.png', '킹갓 엠퍼럴 칫솔', 120000),
    ProductsItem(4, 'assets/product4.png', '강아지 칫솔', 120000)
  ];
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('상품 리스트'),
      ),
      body: ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext ctx, int idx){
          return ComponentProductItem(
              productsItem: _list[idx],
              callback: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageProductDetail(productsItem: _list[idx])));
              }
          );
        },
      ),
    );
  }
}
