import 'package:flutter/material.dart';
import 'package:shopping_app/model/products_item.dart';

class PageProductDetail extends StatefulWidget {
  const PageProductDetail({
    super.key,
    required this.productsItem,
  });

  final ProductsItem productsItem;

  @override
  State<PageProductDetail> createState() => _PageProductDetailState();
}

class _PageProductDetailState extends State<PageProductDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('상품 상세보기'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              child: Image.asset(
                  widget.productsItem.imgUrl,
                width: 500,
                height: 500,
                fit: BoxFit.contain,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
              child: Column(
                children: [
                  Text(
                    widget.productsItem.productTitle,
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    '${widget.productsItem.productPrice}',
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
