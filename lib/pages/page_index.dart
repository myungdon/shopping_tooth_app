import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:shopping_app/components/component_product_frame.dart';
import 'package:shopping_app/pages/page_product_detail1.dart';
import 'package:shopping_app/pages/page_product_detail2.dart';
import 'package:shopping_app/pages/page_product_detail3.dart';
import 'package:shopping_app/pages/page_product_detail4.dart';
import 'package:bottom_bar_with_sheet/bottom_bar_with_sheet.dart';

void main() {
  runApp(const PageIndex());
}

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}



class _PageIndexState extends State<PageIndex> {
  final _bottomBarController = BottomBarWithSheetController(initialIndex: 0);

  @override
  void initState() {
    _bottomBarController.stream.listen((opened) {
      debugPrint('Bottom bar ${opened ? 'opened' : 'closed'}');
    });
    super.initState();
  }

  String searchValue = '';
  final List<String> _suggestions = [
    '고양이 칫솔',
    '카와이 칫솔',
    '킹갓 엠퍼럴 칫솔',
    '강아지 칫솔',
    '나무 칫솔',
    '칫솔 세트',
  ];

  Future<List<String>> _fetchSuggestions(String searchValue) async {
    await Future.delayed(const Duration(milliseconds: 750));

    return _suggestions.where((element) {
      return element.toLowerCase().contains(searchValue.toLowerCase());
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        title: 'Tooth Mall',
        theme: ThemeData(primarySwatch: Colors.orange),
        home: Scaffold(
          appBar: EasySearchBar(
              title: const Text('Tooth Mall'),
              onSearch: (value) => setState(() => searchValue = value),
              actions: [
                IconButton(icon: const Icon(Icons.person), onPressed: () {})
              ],
              asyncSuggestions: (value) async =>
                  await _fetchSuggestions(value)),
          drawer: Drawer(
              child: ListView(padding: EdgeInsets.zero, children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('메뉴'),
            ),
            ListTile(
                title: const Text('내 정보 관리'),
                onTap: () => Navigator.pop(context)),
            ListTile(
                title: const Text('로그아웃'), onTap: () => Navigator.pop(context))
          ])),
          body: _buildBody(context),
          bottomNavigationBar: BottomBarWithSheet(
            controller: _bottomBarController,
            bottomBarTheme: const BottomBarTheme(
              mainButtonPosition: MainButtonPosition.middle,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.vertical(top: Radius.circular(25)),
              ),
              itemIconColor: Colors.grey,
              itemTextStyle: TextStyle(
                color: Colors.grey,
                fontSize: 10.0,
              ),
              selectedItemTextStyle: TextStyle(
                color: Colors.blue,
                fontSize: 10.0,
              ),
            ),
            onSelectItem: (index) => debugPrint('$index'),
            sheetChild: Center(
              child: Text(
                "Another content",
                style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 20,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
            items: const [
              BottomBarWithSheetItem(icon: Icons.people),
              BottomBarWithSheetItem(icon: Icons.shopping_cart),
              BottomBarWithSheetItem(icon: Icons.settings),
              BottomBarWithSheetItem(icon: Icons.favorite),
            ],
          ),
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    return GridView(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 0,
        mainAxisSpacing: 0,
        childAspectRatio: 1.0,
      ),
      children: <Widget>[
        ComponentProductFrame(
            imgUrl: 'assets/product1.png',
            productTitle: '카와이 칫솔',
            productPrice: 120000,
            callback: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const PageProductDetail1()));
            }),
        ComponentProductFrame(
            imgUrl: 'assets/product2.png',
            productTitle: '고양이 칫솔',
            productPrice: 120000,
            callback: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const PageProductDetail2()));
            }),
        ComponentProductFrame(
            imgUrl: 'assets/product3.png',
            productTitle: '킹갓 칫솔',
            productPrice: 120000,
            callback: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const PageProductDetail3()));
            }),
        ComponentProductFrame(
            imgUrl: 'assets/product4.png',
            productTitle: '강아지 칫솔',
            productPrice: 120000,
            callback: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const PageProductDetail4()));
            }),
      ],
    );
  }
}